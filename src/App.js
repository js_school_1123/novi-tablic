import './App.css';
import './App.scss';
import Main from './components/Main';
import Aside from './components/Aside';

function App() {
  return (
    <div className="table">
      <Aside />
      <Main />
    </div>
  );
}

export default App;
