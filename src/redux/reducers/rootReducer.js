import { combineReducers } from '@reduxjs/toolkit';
import { personReducer } from './person/reducer';
import { jobReducer } from './job/reducer';

const rootReducer = combineReducers({
  person: personReducer,
  job: jobReducer,
});

export default rootReducer;
//
// const state = {
//   person: {
//     name: 'Pera',
//     age: 100,
//   },
//   job: {
//     title: 'Dev',
//     salary: 100,
//   },
// };
