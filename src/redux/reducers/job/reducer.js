import { SET_TITLE, SET_SALARY } from './constants';

const jobReducer = (state = { title: '', salary: 0 }, action) => {
  switch (action.type) {
    case SET_TITLE:
      return {
        ...state,
        title: action.payload,
      };
    case SET_SALARY:
      return {
        ...state,
        salary: action.payload,
      };
    default:
      return state;
  }
};

export { jobReducer };
