const SET_NAME = 'SET_NAME';
const SET_AGE = 'SET_AGE';

export { SET_NAME, SET_AGE };
