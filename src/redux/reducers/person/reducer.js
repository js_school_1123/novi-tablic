import { SET_AGE, SET_NAME } from './constants';

const personReducer = (state = { name: '', age: 0 }, action) => {
  switch (action.type) {
    case SET_AGE:
      return {
        ...state,
        age: action.payload,
      };
    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      };
    default:
      return state;
  }
};

export { personReducer };
