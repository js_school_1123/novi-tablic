import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import monitorReducersEnhancer from './enhancers/monitorReducers';
import logger from './middleware/logger';
import rootReducer from './reducers/rootReducer';
import preloadedState from './preloadedState';

// console.log('***', getDefaultMiddleware())
// Firefox says:
// immutableStateInvariantMiddleware
// createThunkMiddleware
// createSerializableStateInvariantMiddleware

const middleware = [...getDefaultMiddleware(), logger];

export default configureStore({
  reducer: rootReducer,
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
  preloadedState,
  enhancers: [monitorReducersEnhancer],
});
