const state = {
  players: [
    {
      id: 123,
      name: 'Profa',
      img: '',
      level: 'Junior',
      hand: [
        {
          image: 'https://deckofcardsapi.com/static/img/KH.png',
          value: 'KING',
          suit: 'HEARTS',
          code: 'KH',
        },
      ],
      pile: [{}],
      notches: 0,
      score: 0,
    },
    {},
  ],
  scores: {},
  playground: {
    hands: [],
    common: [],
    timerinfo: 'Vi ste na potezu',
  },
};

console.log(state);
