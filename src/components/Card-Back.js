const CardBack = () => {
  return <img className="back-of-card" src="https://deckofcardsapi.com/static/img/AS.png" alt="back-of-card" />;
};
export default CardBack;
