const Score = () => {
  return (
    <div className="score">
      <div className="player one">
        <div>
          <div className="name">Profa</div>
          <div>IIII</div>
        </div>
        <div className="single-score">4</div>
      </div>
      <div className="player two">
        <div>
          <div className="name">Javorka</div>
          <div>III</div>
        </div>
        <div className="single-score">3</div>
      </div>
    </div>
  );
};

export default Score;
