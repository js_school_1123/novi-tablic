import CardRow from './Card-Row';

const PlayerGuest = () => {
  return (
    <div className="player-quest">
      <CardRow />
    </div>
  );
};
export default PlayerGuest;
