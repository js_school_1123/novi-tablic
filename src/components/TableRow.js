import SingleCard from './SingleCard';

const TableRow = () => {
  return (
    <div className="table-row">
      <SingleCard />
      <SingleCard />
      <SingleCard />
      <SingleCard />
    </div>
  );
};
export default TableRow;
