import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PlayerScore = (props /*, ctx*/) => {
  const { name, level } = props;

  return (
    <div className="single-player">
      <div className="name">{name}</div>
      <div className="player-info">
        <div className="img"></div>
        <div className="status">{level}</div>
      </div>
    </div>
  );
};

PlayerScore.propTypes = {
  name: PropTypes.string,
  level: PropTypes.string,
};

PlayerScore.defaultProps = {
  name: 'Profa',
  level: 'Junior',
};

const mapStateToProps = state => {
  return {
    name: state.person.name,
    level: state.job.title,
  };
};

export default connect(mapStateToProps)(PlayerScore);
