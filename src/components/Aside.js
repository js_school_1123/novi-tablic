import PlayerScore from './Player-Score';
import Score from './Score';

const Aside = () => {
  return (
    <div className="aside">
      <PlayerScore name={'Javorka'} level={'Senior'} />
      <Score />
      <PlayerScore />
    </div>
  );
};

export default Aside;

// PlayerScore( {name: "Javorka", level: "Senior"}, {...});
