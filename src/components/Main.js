import Playground from './Playground';
import Pile from './Pile';

const Main = () => {
  return (
    <div className="main">
      <Playground />
      <Pile />
    </div>
  );
};

export default Main;
