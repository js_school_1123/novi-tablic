import CardBack from './Card-Back';

const Pile = () => {
  return (
    <div className="deck">
      <CardBack />
      <CardBack />
      <CardBack />
    </div>
  );
};

export default Pile;
