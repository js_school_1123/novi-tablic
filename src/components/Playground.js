import PlayerGuest from './Player-Guest';
import TableRow from './TableRow';
import PlayerHome from './Player-Home';

const Playground = () => {
  return (
    <div className="main-content">
      <PlayerGuest />
      <TableRow />
      <PlayerHome />
    </div>
  );
};
export default Playground;
