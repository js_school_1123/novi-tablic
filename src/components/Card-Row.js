import SingleCard from './SingleCard';

const CardRow = () => {
  return (
    <div className="cards-row">
      <SingleCard />
      <SingleCard />
      <SingleCard />
      <SingleCard />
      <SingleCard />
      <SingleCard />
    </div>
  );
};
export default CardRow;
