import CardRow from './Card-Row';
import TimerInfo from './TimerInfo';

const PlayerHome = () => {
  return (
    <div className="player-home">
      <TimerInfo />
      <CardRow />
    </div>
  );
};
export default PlayerHome;
